import React from "react";
import "./NewExpense.css";
import ExpenseForm from "./ExpenseForm";
import ExpenseDate from "../Expenses/ExpenseDate";

const NewExpense = (props) => {
  const saveExpenceDataHandler = (enteredExpenseData) => {
    const ExpenseData = {
      ...enteredExpenseData,
      id: Math.random().toString(),
    };
    props.onAddExpense(ExpenseData);
  };

  return (
    <div className="new-expense">
      <ExpenseForm onSaveExpenseData={saveExpenceDataHandler} />
    </div>
  );
};

export default NewExpense;
