import React from "react";
import Card from "../UI/Card";
import styles from "./UserList.module.css";

const UserList = (props) => {
  return (
    <div>
      <Card className={styles.users}>
        <ul>
          {props.users.map((user) => (
            <li key={user.id}>
              I am {user.name} {user.age} year old.
            </li>
          ))}
        </ul>
      </Card>
    </div>
  );
};

export default UserList;
