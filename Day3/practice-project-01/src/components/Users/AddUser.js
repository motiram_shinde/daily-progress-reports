import React, { useState } from "react";
// import Button from "../UI/Button";
import Card from "../UI/Card";
import styles from "./AddUser.module.css";
// import { TextField } from "@mui/material-ui/core";
import Button from "@material-ui/core/Button";
import { TextField } from "@material-ui/core";
import ErrorModal from "../UI/ErrorModal";
import Wrapper from "../Helpers/Wrapper";

const AddUser = (props) => {
  const [enteredUsername, setEnteredUsername] = useState("");
  const [enteredAge, setEnteredAge] = useState("");
  const [error, setError] = useState("");

  const addUsername = (event) => {
    setEnteredUsername(event.target.value);
  };

  const addUserage = (event) => {
    setEnteredAge(event.target.value);
  };
  const submiHandler = (event) => {
    event.preventDefault();
    if (enteredUsername.trim().length === 0 || enteredAge.trim().length === 0) {
      setError({
        errorTitle: "Invalid input",
        message:
          "Please enter a valid name and age (Value Should be non empty)",
      });
      return;
    }
    if (+enteredAge < 1) {
      setError({
        errorTitle: "Invalid age",
        message: "Please enter a valid age (Value Should be greater than 0)",
      });
      return;
    }
    props.addUser(enteredUsername, enteredAge);
    // console.log(enteredUsername, enteredAge);
    setEnteredUsername("");
    setEnteredAge("");
  };

  const errorHandler = () => {
    setError(null);
  };

  return (
    <Wrapper>
      {error && (
        <ErrorModal
          errorTitle={error.errorTitle}
          message={error.message}
          onConfirm={errorHandler}
        />
      )}
      <Card className={styles.input}>
        <form onSubmit={submiHandler}>
          {/* <label>Username</label> */}
          <TextField
            id="standard-basic"
            label="Username"
            variant="standard"
            value={enteredUsername}
            onChange={addUsername}
          />
          <br />
          {/* <input
          type="text"
          value={enteredUsername}
          onChange={addUsername}
          placeholder="Username"
        ></input> */}
          {/* <label>Age</label> */}
          <TextField
            id="standard-basic"
            label="Age"
            variant="standard"
            value={enteredAge}
            onChange={addUserage}
          />
          <br />
          <br />
          {/* <input
          value={enteredAge}
          type="number"
          onChange={addUserage}
          placeholder="age"
        ></input> */}
          {/* <Button type="submit">Add User</Button> */}
          <Button variant="contained" color="success" type="submit">
            Add User
          </Button>
        </form>
      </Card>
    </Wrapper>
  );
};

export default AddUser;
