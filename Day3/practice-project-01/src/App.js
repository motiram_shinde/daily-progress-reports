import React, { useState, Fragment } from "react";
import AddUser from "./components/Users/AddUser";
import UserList from "./components/Users/UserList";

function App() {
  const [userList, setUserList] = useState([]);
  const AddUserHandler = (UsName, UsAge) => {
    setUserList((prev) => {
      return [
        ...prev,
        { id: Math.random().toString(), name: UsName, age: UsAge },
      ];
    });
  };
  return (
    <Fragment>
      <AddUser addUser={AddUserHandler} />
      <UserList users={userList} />
    </Fragment>
  );
}

export default App;
