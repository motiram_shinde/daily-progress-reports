import React, { useCallback, useState } from "react";

import "./App.css";
import DemoOutput from "./components/Demo/DemoOutput";
import Button from "./components/UI/Button/Button";

function App() {
  const [showParagraph, setShowParagraph] = useState(false);
  const [allowToggale, setAllowToggale] = useState(false);

  console.log("App Running");

  const toggaleParagraphHandler = useCallback(() => {
    if (allowToggale) {
      setShowParagraph((prev) => !prev);
    }
  }, [allowToggale]);

  const allowToggaleHandler = () => {
    setAllowToggale(true);
  };

  return (
    <div className="app">
      <h1>Hi there!</h1>
      <DemoOutput show={showParagraph} />
      <Button onClick={allowToggaleHandler}>Allow Toggle</Button>
      <Button onClick={toggaleParagraphHandler}>Show Paragraph</Button>
    </div>
  );
}

export default App;
